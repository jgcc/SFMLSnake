#include "GameWindow.h"

GameWindow::GameWindow() {
	setup("Demo Window", sf::Vector2u(800,600));
}

GameWindow::GameWindow(const std::string& tittle, const sf::Vector2u& size) {
	setup(tittle, size);
}

GameWindow::~GameWindow() {
	destroy();
}

void GameWindow::BeginDraw() { mWindow.clear(sf::Color(15, 15, 15, 255)); }
void GameWindow::EndDraw() { mWindow.display(); }
bool GameWindow::IsDone() { return isDone; }
bool GameWindow::IsFullScreen() { return isFullScreen; }
sf::Vector2u GameWindow::getWindowSize() { return mWindowSize; }
sf::RenderWindow *GameWindow::getRendererWindow() { return &mWindow; }


void GameWindow::update() {
	sf::Event event;
	while (mWindow.pollEvent(event)) {
		if (event.type == sf::Event::Closed)
		{
			isDone = true;
		} else if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::F5)
		{
			toggleFullScreen();
		}
	}
}

void GameWindow::toggleFullScreen() {
	isFullScreen = !isFullScreen;
	destroy();
	create();
}

void GameWindow::draw(sf::Drawable &drawable) {
	mWindow.draw(drawable);
}

void GameWindow::setup(const std::string& tittle, const sf::Vector2u size) {
	mWindowTittle = tittle;
	mWindowSize = size;
	isFullScreen = false;
	isDone = false;
	create();
}

void GameWindow::destroy() {
	mWindow.close();
}

void GameWindow::create() {
	auto style = (isFullScreen ? sf::Style::Fullscreen : sf::Style::Default);
	mWindow.create({mWindowSize.x, mWindowSize.y, 32}, mWindowTittle, style);
}
