#include "World.h"

World::World(sf::Vector2u windowSize)
{
	blockSize = 16;

	this->windowSize = windowSize;
	respawnApple();
	apple.setFillColor(sf::Color::Red);
	apple.setRadius(blockSize / 2);

	for (int i = 0; i < 4; i++)
	{
		bounds[i].setFillColor(sf::Color(155,10,60));
		if (!((i +1) % 2)) {
			bounds[i].setSize(sf::Vector2f(windowSize.x, blockSize));
		} else {
			bounds[i].setSize(sf::Vector2f(blockSize, windowSize.y));
		}
		if (i < 2) {
			bounds[i].setPosition(0,0);
		} else {
			bounds[i].setOrigin(bounds[i].getSize());
			bounds[i].setPosition(sf::Vector2f(windowSize));
		}
	}
}

World::~World()
{
}

int World::getBlockSize()
{
	return blockSize;
}

void World::respawnApple()
{
	int maxX = (windowSize.x / blockSize) - 2;
	int maxY = (windowSize.y / blockSize) - 2;
	item = sf::Vector2i(rand() % maxX + 1, rand() % maxY + 1);
	apple.setPosition(item.x * blockSize, item.y * blockSize);
}

void World::render(sf::RenderWindow & window)
{
	for (int i = 0; i < 4; i++)
	{
		window.draw(bounds[i]);
	}
	window.draw(apple);
}
