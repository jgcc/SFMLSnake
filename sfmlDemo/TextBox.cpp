#include "TextBox.h"



TextBox::TextBox() {
	setup(5, 9, 200, sf::Vector2f(0,0));
}

TextBox::TextBox(int visible, int charSize, int width, sf::Vector2f screenPos) {
	setup(visible, charSize, width, screenPos);
}


TextBox::~TextBox() {
	clear();
}

void TextBox::setup(int visible, int charSize, int width, sf::Vector2f screenPos) {
	numVisible = visible;
	
	sf::Vector2f offset(2.0f, 2.0f);
	font.loadFromFile("arial.ttf");
	content.setFont(font);
	content.setString("");
	content.setCharacterSize(charSize);
	content.setPosition(screenPos + offset);

	backdrop.setSize(sf::Vector2f(width, visible * (charSize * 1.2f)));
	backdrop.setFillColor(sf::Color(90,90,90,90));
	backdrop.setPosition(screenPos);
}

void TextBox::add(std::string msg) {
	message.push_back(msg);
	if (message.size() < 6) return;
	message.erase(message.begin());
}

void TextBox::clear() { message.clear(); }

void TextBox::render(sf::RenderWindow &window) {
	std::string tmp_content;
	
	for (auto &itr : message) {
		tmp_content.append(itr+"\n");
	}

	if (tmp_content != ""){
		content.setString(tmp_content);
		window.draw(backdrop);
		window.draw(content);
	}
}
