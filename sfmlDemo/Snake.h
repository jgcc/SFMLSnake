#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
//#include "Textbox.h"

struct SnakeSegmet
{
	SnakeSegmet(int x, int y) : position(x, y) {}
	sf::Vector2i position;
};

using SnakeContainer = std::vector<SnakeSegmet>;
enum class Direction { None, Up, Down, Left, Right };

class Snake
{
public:
	Snake(int blockSize);
	~Snake();			

	void setDirection(Direction);
	Direction getDirection();
	int getSpeed();
	void setSpeed(int);
	sf::Vector2i getPosition();
	int getLives();
	int getScore();
	void increaseScore();
	bool hasLost();

	void lose();	// handle lose
	void toggleLose();

	void extend();	// grow the snake
	void reset();

	void move();
	void tick();
	void cut(int);
	void render(sf::RenderWindow &mWindow);

private:
	void checkCollision();

	SnakeContainer snakeBody;
	int size;
	Direction dir;
	int speed;
	int lives;
	int score;
	bool lost;
	sf::RectangleShape bodyRect;
	//Textbox *log;

};

