#include "Snake.h"

Snake::Snake(int blockSize)
{
	size = blockSize;
	bodyRect.setSize(sf::Vector2f(size-1, size-1));
	reset();
}
Snake::~Snake(){}

void Snake::setDirection(Direction dir) { this->dir = dir; }
int Snake::getSpeed(){return speed;}
void Snake::setSpeed(int _speed) { speed=_speed; }
sf::Vector2i Snake::getPosition(){return (!snakeBody.empty() ? snakeBody.front().position : sf::Vector2i(1, 1));}

int Snake::getLives(){return lives;}
int Snake::getScore(){return score;}

void Snake::increaseScore(){score += 10;}
bool Snake::hasLost(){return lost;}
void Snake::lose(){lost = true;}
void Snake::toggleLose(){lost = !lost;}

Direction Snake::getDirection() {
	if (snakeBody.size() <= 1) return Direction::None;

	auto &head = snakeBody[0];
	auto &neck = snakeBody[1];

	if (head.position.x == neck.position.x ){
		return head.position.y > neck.position.y ? Direction::Up : Direction::Down;
	}else if(head.position.y == neck.position.y){
		return head.position.x > neck.position.x ? Direction::Right : Direction::Left;
	}

	return Direction::None;
}

void Snake::extend()
{
	if (snakeBody.empty()) { return; }

	SnakeSegmet &tail_head = snakeBody[snakeBody.size() - 1];

	if (snakeBody.size() > 1)
	{
		SnakeSegmet &tail_bone = snakeBody[snakeBody.size() - 2];
		if (tail_head.position.x == tail_bone.position.x) {
			if (tail_head.position.y > tail_bone.position.y) {
				snakeBody.push_back(SnakeSegmet(tail_head.position.x, tail_head.position.y + 1));
			} else {
				snakeBody.push_back(SnakeSegmet(tail_head.position.x, tail_head.position.y - 1));
			}
		}
		else if(tail_head.position.y == tail_bone.position.y){
			if (tail_head.position.x > tail_bone.position.x) {
				snakeBody.push_back(SnakeSegmet(tail_head.position.x + 1, tail_head.position.y));
			} else {
				snakeBody.push_back(SnakeSegmet(tail_head.position.x - 1, tail_head.position.y));
			}
		}
	} else {
		switch (dir) {
		Up:
			snakeBody.push_back(SnakeSegmet(tail_head.position.x, tail_head.position.y + 1));
			break;
		Down:
			snakeBody.push_back(SnakeSegmet(tail_head.position.x, tail_head.position.y - 1));
			break;
		Left:
			snakeBody.push_back(SnakeSegmet(tail_head.position.x + 1, tail_head.position.y));
			break;
		Right:
			snakeBody.push_back(SnakeSegmet(tail_head.position.x - 1, tail_head.position.y));
			break;
		default:
			break;
		}

	}
}

void Snake::reset()
{
	snakeBody.clear();

	snakeBody.push_back(SnakeSegmet(5,7));
	snakeBody.push_back(SnakeSegmet(5,6));
	snakeBody.push_back(SnakeSegmet(5,5));

	setDirection(Direction::None);
	speed = 5;
	lives = 3;
	score = 0;
	lost = false;
}

void Snake::move()
{
	for (int i = snakeBody.size() - 1; i > 0; i--) {
		snakeBody[i].position = snakeBody[i - 1].position;
	}
	
	switch (dir)
	{	
	case Direction::Up:
		--snakeBody[0].position.y;
		break;
	case Direction::Down:
		++snakeBody[0].position.y;
		break;
	case Direction::Left:
		--snakeBody[0].position.x;
		break;
	case Direction::Right:
		++snakeBody[0].position.x;
		break;
	}
}

void Snake::tick()
{
	if (snakeBody.empty()) return;
	if (dir == Direction::None) return;
	move();
	checkCollision();
}

void Snake::cut(int segments)
{
	for (int i = 0; i < segments; i++)
	{
		snakeBody.pop_back();
	}
	lives--;
	if (!lives){
		lose();
		return;
	}
}

void Snake::render(sf::RenderWindow &mWindow)
{
	if (snakeBody.empty()) {
		return;
	}
	auto head = snakeBody.begin();
	bodyRect.setFillColor(sf::Color::Yellow);
	bodyRect.setPosition(head->position.x * size, head->position.y * size);
	mWindow.draw(bodyRect);

	bodyRect.setFillColor(sf::Color::Green);
	for (auto itr = snakeBody.begin() + 1; itr != snakeBody.end(); itr++)
	{
		bodyRect.setPosition(itr->position.x * size, itr->position.y * size);
		mWindow.draw(bodyRect);
	}
}

void Snake::checkCollision()
{
	if (snakeBody.size() < 5) { return; }
	SnakeSegmet &head = snakeBody.front();
	for (auto itr = snakeBody.begin() + 1; itr != snakeBody.end(); itr++) {
		if (itr->position == head.position)
		{
			int segments = snakeBody.end() - itr;
			cut(segments);
			break;
		}
	}
}
