#pragma once
#include <SFML\Graphics.hpp>
class TextBox
{

	using MessageContainer = std::vector<std::string>;

public:
	TextBox();
	TextBox(int, int, int, sf::Vector2f);
	~TextBox();

	void setup(int, int, int, sf::Vector2f);
	void add(std::string);
	void clear();
	void render(sf::RenderWindow&);

private:
	MessageContainer message;
	int numVisible;
	sf::RectangleShape backdrop;
	sf::Font font;
	sf::Text content;

};

