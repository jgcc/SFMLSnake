#pragma once
#include <string>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

class GameWindow {
public:
	GameWindow();
	GameWindow(const std::string& tittle, const sf::Vector2u& size);
	~GameWindow();

	void BeginDraw();	//Clear the window
	void EndDraw();		// Display the changes

	void update();

	bool IsDone();
	bool IsFullScreen();
	sf::RenderWindow *getRendererWindow();
	sf::Vector2u getWindowSize();

	void toggleFullScreen();

	void draw(sf::Drawable &drawable);
private:
	void setup(const std::string &tittle, const sf::Vector2u size);
	void destroy();
	void create();

	sf::RenderWindow mWindow;
	sf::Vector2u mWindowSize;
	std::string mWindowTittle;
	bool isDone;
	bool isFullScreen;
};

