#include "Game.h"

Game::Game(): mWindow("Snake", sf::Vector2u(800,600)),
player(mWorld.getBlockSize()), mWorld(sf::Vector2u(800,600))
{
	mTextBox.setup(5, 14, 350, sf::Vector2f(255.f, 0.0f));
}

void Game::RestartClock() { m_elapsed += m_clock.restart(); }
GameWindow * Game::GetWindow() { return &mWindow; }
sf::Time Game::GetElapsed() { return m_elapsed; }
Game::~Game() {}

void Game::HandleInput()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && player.getDirection() != Direction::Down) {
		player.setDirection(Direction::Up);
	} else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && player.getDirection() != Direction::Up) {
		player.setDirection(Direction::Down);
	} else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && player.getDirection() != Direction::Right) {
		player.setDirection(Direction::Left);
	} else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && player.getDirection() != Direction::Left) {
		player.setDirection(Direction::Right);
	}
}

void Game::Update()
{
	mWindow.update();
	float timestep = 1.0f / player.getSpeed();
	if (m_elapsed.asSeconds() >= timestep)
	{
		player.tick();
		mWorld.update(player);
		m_elapsed -= sf::seconds(timestep);
		if (player.hasLost())
		{
			mTextBox.add("Game over score: " + std::to_string(player.getScore()));
			player.reset();
		}
	}	
}

void Game::Render()
{
	mWindow.BeginDraw();
	mWorld.render(*mWindow.getRendererWindow());
	player.render(*mWindow.getRendererWindow());
	mTextBox.render(*mWindow.getRendererWindow());
	mWindow.EndDraw();
}
