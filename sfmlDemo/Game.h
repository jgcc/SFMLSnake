#pragma once
#include "GameWindow.h"
#include "TextBox.h"
#include "World.h"


class Game
{
public:
	Game();
	~Game();

	void HandleInput();
	void Update();
	void Render();	
	void RestartClock();
	GameWindow *GetWindow();
	sf::Time GetElapsed();
	TextBox mTextBox;

private:
	GameWindow mWindow;
	sf::Clock m_clock;
	sf::Time m_elapsed;

	World mWorld;
	Snake player;

	sf::Texture m_chaickaTexture;
	sf::Sprite m_Chaicka;
	sf::Vector2i increment;
};
