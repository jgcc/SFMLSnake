#pragma once
#include "Snake.h"

class World
{
public:
	World(sf::Vector2u windowSize);
	~World();

	int getBlockSize();
	void respawnApple();
	void update(Snake &player)
	{
		if (player.getPosition() == item) {
			player.extend();
			player.increaseScore();
			player.setSpeed(player.getSpeed() + 1);
			respawnApple();
		}

		int gridSize_x = windowSize.x / blockSize;
		int gridSize_y = windowSize.y / blockSize;

		if (player.getPosition().x <= 0 ||
			player.getPosition().y <= 0 ||
			player.getPosition().x >= gridSize_x - 1 ||
			player.getPosition().y >= gridSize_y - 1) {
			player.lose();
		}
	}
	void render(sf::RenderWindow &window);

private:
	sf::Vector2u windowSize;
	sf::Vector2i item;
	int blockSize;

	sf::CircleShape apple;
	sf::RectangleShape bounds[4];
};

